package cz.alabanda.positionsharingapp.services

import android.app.Service
import android.content.Intent
import android.os.IBinder
import cz.alabanda.positionsharingapp.repository.network.RestApiClient
import java.util.*

class LiveDataService : Service() {
    private val intervalBetweenDataUpdatesInMs: Long = 2000
    private val timer: Timer = Timer()

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        startLiveDataUpdates()
        return START_NOT_STICKY;
    }

    private fun startLiveDataUpdates() {
        timer.schedule(
            object : TimerTask() {
                override fun run() {
                    RestApiClient.getGrantedPermissions()
                }
            },
            intervalBetweenDataUpdatesInMs,
            intervalBetweenDataUpdatesInMs
        )

        timer.schedule(
            object : TimerTask() {
                override fun run() {
                    RestApiClient.getReceivedPermissions()
                }
            },
            intervalBetweenDataUpdatesInMs,
            intervalBetweenDataUpdatesInMs
        )
    }

    override fun onDestroy() {
        super.onDestroy()
        timer.cancel()
    }

    override fun onBind(intent: Intent): IBinder? {
        return null
    }
}