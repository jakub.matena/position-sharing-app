package cz.alabanda.positionsharingapp.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import cz.alabanda.positionsharingapp.R
import cz.alabanda.positionsharingapp.fragments.WantedUsersFragment

class UsernameAndButtonAdapter(repositories: List<String>, private val action: (m: String) -> Unit, private val actionText : String) :
    RecyclerView.Adapter<UsernameAndButtonAdapter.ViewHolder>() {

    var context: Context? = null

    var repositories = repositories
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    /**
     * Provide a reference to the type of views that you are using
     * (custom ViewHolder).
     */
    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val usernameTextView: TextView
        val actionButton: Button

        init {
            // Define click listener for the ViewHolder's View.
            usernameTextView = view.findViewById(R.id.usernameTextView)
            actionButton = view.findViewById(R.id.actionButton)
        }
    }

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        // Create a new view, which defines the UI of the list item
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.username_and_button_view_item, viewGroup, false)

        context = view.context
        return ViewHolder(view)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {

        // Get element from your dataset at this position and replace the
        // contents of the view with that element
        val name = repositories[position]
        viewHolder.usernameTextView.text = name
        viewHolder.actionButton.text = actionText

        viewHolder.actionButton.setOnClickListener({action(name)})
    }

    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount() = repositories.size

}
