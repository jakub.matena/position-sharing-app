package cz.alabanda.positionsharingapp.fragments

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.observe
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import cz.alabanda.positionsharingapp.R
import cz.alabanda.positionsharingapp.adapters.UsernameAndButtonAdapter
import cz.alabanda.positionsharingapp.repository.network.RestApiClient
import cz.alabanda.positionsharingapp.data.Permission
import cz.alabanda.positionsharingapp.utils.ListComparer
import cz.alabanda.positionsharingapp.utils.ViewModelResponseState
import kotlinx.android.synthetic.main.fragment_granted_permissions.*
import kotlinx.android.synthetic.main.fragment_watched_users.*
import java.util.*

/**
 * A simple [Fragment] subclass.
 * Use the [GrantedPermissionsFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class GrantedPermissionsFragment : Fragment() {


    lateinit var grantedPermissionsAdapter: UsernameAndButtonAdapter

    /**
     * Inflate view hierarchy to this fragment.
     */
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_granted_permissions, container, false)
    }

    override fun onStart() {
        super.onStart()
        grantedPermissionsAdapter = UsernameAndButtonAdapter(
            Collections.emptyList(),
            { revokePermission(it) },
            getString(R.string.global_revoke)
        )

        grantedPermissionsView.adapter = grantedPermissionsAdapter
        grantedPermissionsView.layoutManager = LinearLayoutManager(requireContext())

        RestApiClient.prepareGetGrantedPermissions().observe(viewLifecycleOwner) {
            when (it) {
                is ViewModelResponseState.Error -> handleError(it.error)
                is ViewModelResponseState.Success -> handleGrantedPermissions(it.content)
            }
        }
        RestApiClient.getGrantedPermissions()
    }

    private fun handleGrantedPermissions(repositories: List<Permission>) {
        val oldData = grantedPermissionsAdapter.repositories
        val newData = repositories.filter { it.approved }.map { user -> user.name }
        if (!ListComparer.isEqual(oldData, newData))
            grantedPermissionsAdapter.repositories = newData
    }

    private fun handleError(error: String) {
        val message = "Failed to download granted permissions: $error"
        Log.i("TAG", message)
        val failure = Toast.makeText(
            context,
            message,
            Toast.LENGTH_SHORT
        )
        failure.show()
    }

    fun revokePermission(username: String) {
        RestApiClient.prepareRefusePermission().observe(viewLifecycleOwner) {
            when (it) {
                is ViewModelResponseState.Error -> handlePermissionRefusalError(it.error)
                is ViewModelResponseState.Success -> handlePermissionRefused()
            }
        }

        RestApiClient.refusePermission(username)
    }

    private fun handlePermissionRefused() {
        // Show toast
        val success = Toast.makeText(context, "Permission refused", Toast.LENGTH_SHORT)
        success.show()

        // Refresh list
        RestApiClient.getGrantedPermissions()
    }

    private fun handlePermissionRefusalError(error: String) {
        val message = "Permission refusal failed: $error"
        Log.i("TAG", message)
        // Show toast
        val failure = Toast.makeText(
            context,
            message,
            Toast.LENGTH_SHORT
        )
        failure.show()
    }

    /**
     * Stop any [Glide] operations related to this fragment.
     */
    override fun onStop() {
        super.onStop()
        Glide.with(this).onStop()
    }

    companion object {

        /**
         * Factory method to create fragment instance. Framework requires empty default constructor.
         */
        @JvmStatic
        fun newInstance(): GrantedPermissionsFragment {
            val fragment = GrantedPermissionsFragment()

            return fragment
        }
    }
}

