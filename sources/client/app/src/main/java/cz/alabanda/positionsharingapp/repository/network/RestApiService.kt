package cz.alabanda.positionsharingapp.repository.network

import cz.alabanda.positionsharingapp.data.Permission
import cz.alabanda.positionsharingapp.data.UserLocation
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.POST
import retrofit2.http.Query

interface RestApiService {

    @Headers("Accept: application/json")
    @GET("permission/received")
    suspend fun getReceivedPermissions(@Query("token") token: String): List<Permission>?

    @Headers("Accept: application/json")
    @GET("user/token")
    suspend fun login(@Query("name") name: String, @Query("password") password: String): String?

    @Headers("Accept: application/json")
    @POST("user")
    suspend fun createUser(@Query("name") name: String, @Query("password") password: String)

    @Headers("Accept: application/json")
    @POST("permission/received")
    suspend fun requestPermission(@Query("token") token: String, @Query("watchedUserName") watchedUserName: String)

    @Headers("Accept: application/json")
    @POST("permission/received/cancel")
    suspend fun cancelPermission(@Query("token") token: String, @Query("watchedUserName") watchedUserName: String)

    @Headers("Accept: application/json")
    @POST("location")
    suspend fun sendLocation(@Query("token") token: String, @Query("time") time: String,
                             @Query("latitude") latitude: String, @Query("longitude") longitude: String)

    @Headers("Accept: application/json")
    @GET("location")
    suspend fun getUserPastLocations(@Query("token") token: String, @Query("watchedUserIds") watchedUserIds: List<Int>,
                                     @Query("maxResults") maxResults: Int): List<UserLocation>

    @Headers("Accept: application/json")
    @POST("permission/granted/grant")
    suspend fun grantPermission(@Query("token") token: String, @Query("grantedUserName") grantedUserName: String)

    @Headers("Accept: application/json")
    @POST("permission/granted/refuse")
    suspend fun refusePermission(@Query("token") token: String, @Query("refusedUserName") refusedUserName: String)

    @Headers("Accept: application/json")
    @GET("permission/granted")
    suspend fun getGrantedPermissions(@Query("token") token: String): List<Permission>?
}