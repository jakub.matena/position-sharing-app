package cz.alabanda.positionsharingapp.data

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Permission(val id: Int,
                      val name: String,
                      val approved: Boolean)