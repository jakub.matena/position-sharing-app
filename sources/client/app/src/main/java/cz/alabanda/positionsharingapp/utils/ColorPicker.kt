package cz.alabanda.positionsharingapp.utils

import androidx.core.graphics.ColorUtils.HSLToColor
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import kotlin.random.Random

class ColorPicker {
    private val colors = listOf(
        BitmapDescriptorFactory.HUE_AZURE,
        BitmapDescriptorFactory.HUE_ROSE,
        BitmapDescriptorFactory.HUE_GREEN,
        BitmapDescriptorFactory.HUE_MAGENTA,
        BitmapDescriptorFactory.HUE_ORANGE,
        BitmapDescriptorFactory.HUE_RED,
        BitmapDescriptorFactory.HUE_VIOLET,
        BitmapDescriptorFactory.HUE_BLUE,
        BitmapDescriptorFactory.HUE_CYAN,
        BitmapDescriptorFactory.HUE_YELLOW
    )

    private val offset = Random.nextInt(colors.count())

    fun getHue(position: Int) : Float {
        return colors[(position + offset) % colors.size]
    }

    fun getColor(position: Int): Int {
        val hue = getHue(position)
        val saturation = 0.9f
        val lightning = 0.5f

        return HSLToColor(floatArrayOf(hue, saturation, lightning))
    }
}