package cz.alabanda.positionsharingapp.services

import android.app.Service
import android.content.Intent
import android.os.IBinder
import cz.alabanda.positionsharingapp.activities.MapActivity
import cz.alabanda.positionsharingapp.repository.network.RestApiClient
import java.util.*

class LiveMapDataService : Service() {
    private val intervalBetweenDataUpdatesInMs : Long = 2000
    private val timer : Timer = Timer()

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        val ids = intent?.getIntegerArrayListExtra(MapActivity.watchedUsersIdsExtraName) ?: ArrayList()
        startLiveDataUpdates(ids)

        return START_NOT_STICKY;
    }

    private fun startLiveDataUpdates(ids: ArrayList<Int>) {
        timer.schedule(
            object : TimerTask() {
                override fun run() {
                    RestApiClient.pullUsersPastLocations(ids)
                }
            },
            intervalBetweenDataUpdatesInMs,
            intervalBetweenDataUpdatesInMs)
    }

    override fun onDestroy() {
        super.onDestroy()
        timer.cancel()
    }

    override fun onBind(p0: Intent?): IBinder? {
        return null
    }
}