package cz.alabanda.positionsharingapp.activities

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.NavUtils
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.LocationServices
import com.google.android.gms.location.LocationSettingsRequest
import com.google.android.gms.location.LocationSettingsResponse
import com.google.android.gms.location.SettingsClient
import com.google.android.gms.tasks.Task
import cz.alabanda.positionsharingapp.services.LocationService
import cz.alabanda.positionsharingapp.R
import cz.alabanda.positionsharingapp.dialogs.UsernameDialogFragment
import cz.alabanda.positionsharingapp.fragments.MenuFragment
import cz.alabanda.positionsharingapp.fragments.WantedUsersFragment
import cz.alabanda.positionsharingapp.fragments.WatchedUsersFragment
import cz.alabanda.positionsharingapp.services.LiveDataService
import kotlinx.android.synthetic.main.activity_watched_users.*

class WatchedUsersActivity : AppCompatActivity() {

    private val turnOnLocationRequestCode = 21

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_watched_users)

        supportFragmentManager.beginTransaction().add(
            R.id.menu_fragment_container,
            MenuFragment.newInstance()
        ).commit()
        supportFragmentManager.beginTransaction().add(
            R.id.watched_fragment_container,
            WatchedUsersFragment.newInstance()
        ).commit()
        supportFragmentManager.beginTransaction().add(
            R.id.wanted_fragment_container,
            WantedUsersFragment.newInstance()
        ).commit()

        requestPermissionFAB.setOnClickListener { showDialog() }
        watchButton.setOnClickListener { showMap() }

        setupNavigation()
        requestLocationPermissions()
        turnOnLocationAndStartLocationService()
        startLiveDataService()
    }
    private fun showMap() {
        val intent = Intent(this, MapActivity::class.java)

        val watchedUsersFragment = supportFragmentManager.findFragmentById(R.id.watched_fragment_container) as WatchedUsersFragment
        val watchedUsersIds = watchedUsersFragment.getWatchedUsersIds()
        intent.putIntegerArrayListExtra(MapActivity.watchedUsersIdsExtraName, ArrayList(watchedUsersIds))

        if (watchedUsersIds.isEmpty()) {
            Toast.makeText(this, "Choose at least one user to watch.", Toast.LENGTH_LONG).show()
            return
        }

        startActivity(intent)
    }

    private fun startLiveDataService() {
        Intent(this, LiveDataService::class.java).also { intent ->
            startService(intent)
        }
    }

    private fun requestLocationPermissions() {
        requestPermissions(arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), 0)
    }

    private fun turnOnLocationAndStartLocationService() {
        val task = assertLocationIsOn()

        task.addOnSuccessListener {
            startLocationService()
        }

        task.addOnFailureListener { exception ->
            if (exception is ResolvableApiException) {
                showDialogToTurnOnLocation(exception)
            }
        }
    }

    private fun assertLocationIsOn(): Task<LocationSettingsResponse> {
        val locationRequest = LocationService.locationRequest()
        val builder = LocationSettingsRequest.Builder().addLocationRequest(locationRequest)
        val client: SettingsClient = LocationServices.getSettingsClient(this)

        return client.checkLocationSettings(builder.build())
    }

    private fun showDialogToTurnOnLocation(exception: ResolvableApiException) {
        exception.startResolutionForResult(this@WatchedUsersActivity, turnOnLocationRequestCode)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == turnOnLocationRequestCode && resultCode == Activity.RESULT_CANCELED) {
            onTurnOnLocationActivityResult(resultCode);
        }
    }

    private fun onTurnOnLocationActivityResult(resultCode: Int) {
        if (resultCode == Activity.RESULT_CANCELED) {
            finish()
        }

        startLocationService()
    }

    private fun startLocationService() {
        Intent(this, LocationService::class.java).also { intent ->
            startService(intent)
        }
    }

    /**
     * Reaction to the menu item click. In this case it is used just for "UP" navigation.
     */
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                NavUtils.navigateUpFromSameTask(this)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun showDialog() {
        val wantedUsersFragment = supportFragmentManager.findFragmentById(R.id.wanted_fragment_container) as WantedUsersFragment
        val newFragment = UsernameDialogFragment (
            { username: String -> wantedUsersFragment.requestPermission(username) },
            getString(R.string.request_permission_label),
            getString(R.string.request_permission_accept_button_text),
            getString(R.string.request_permission_cancel_button_text)
        )
        newFragment.show(supportFragmentManager, "request")
    }

    /**
     * Show navigation up button
     */
    private fun setupNavigation() {
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }
}