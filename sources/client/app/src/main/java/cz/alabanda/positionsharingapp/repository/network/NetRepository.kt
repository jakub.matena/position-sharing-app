package cz.alabanda.positionsharingapp.repository.network

import cz.alabanda.positionsharingapp.data.Permission
import cz.alabanda.positionsharingapp.data.UserLocation
import cz.alabanda.positionsharingapp.repository.Repository
import cz.alabanda.positionsharingapp.utils.Provisions
import retrofit2.HttpException
import java.io.IOException

class NetRepository : Repository {

    override suspend fun getReceivedPermissions(token: String): Result<List<Permission>?> {
        return safeApiCall { Provisions.restApiRepository.getReceivedPermissions(token) }
    }

    override suspend fun createUser(name: String, password: String): Result<Boolean>{
        return noReturnValueApiCall { Provisions.restApiRepository.createUser(name, password)}
    }

    override suspend fun login(name: String, password: String): Result<String?> {
        return safeApiCall { Provisions.restApiRepository.login(name, password) }
    }

    override suspend fun requestPermission(token: String, watchedUserName: String): Result<Boolean> {
        return noReturnValueApiCall { Provisions.restApiRepository.requestPermission(token, watchedUserName) }
    }

    override suspend fun cancelPermission(token: String, watchedUserName: String): Result<Boolean> {
        return noReturnValueApiCall { Provisions.restApiRepository.cancelPermission(token, watchedUserName) }
    }

    override suspend fun sendLocation(token: String, time: String, latitude: String, longitude: String): Result<Boolean> {
        return noReturnValueApiCall { Provisions.restApiRepository.sendLocation(token, time, latitude, longitude) }
    }

    override suspend fun getUserPastLocations(token: String, userIds: List<Int>, maxResults: Int): Result<List<UserLocation>> {
        return safeApiCall { Provisions.restApiRepository.getUserPastLocations(token, userIds, maxResults) }
    }

    override suspend fun grantPermission(token: String, grantedUserName: String): Result<Boolean> {
        return noReturnValueApiCall { Provisions.restApiRepository.grantPermission(token, grantedUserName) }
    }

    override suspend fun refusePermission(token: String, refusedUserName: String): Result<Boolean> {
        return noReturnValueApiCall { Provisions.restApiRepository.refusePermission(token, refusedUserName) }
    }

    override suspend fun getGrantedPermissions(token: String): Result<List<Permission>?> {
        return safeApiCall { Provisions.restApiRepository.getGrantedPermissions(token) }
    }
}

internal suspend fun noReturnValueApiCall(apiCall: suspend () -> Unit): Result<Boolean> {
    return try {
        apiCall.invoke()
        Result.Success(true)
    } catch (t: Throwable) {
        when (t) {
            is IOException -> Result.NetworkError(t)
            is HttpException -> Result.GenericError(t)
            is Exception -> Result.GenericError(t)
            else -> throw(t)
        }
    }
}

internal suspend fun <T> safeApiCall(apiCall: suspend () -> T): Result<T> {
    return try {
        Result.Success(apiCall.invoke())
    } catch (t: Throwable) {
        when (t) {
            is IOException -> Result.NetworkError(t)
            is HttpException -> Result.GenericError(t)
            is Exception -> Result.GenericError(t)
            else -> throw(t)
        }
    }
}

sealed class Result<out T> {
    data class Success<out T>(val value: T): Result<T>()
    data class GenericError(val exception: Exception): Result<Nothing>()
    data class NetworkError(val exception: Exception): Result<Nothing>()
}