package cz.alabanda.positionsharingapp.repository

import cz.alabanda.positionsharingapp.data.Permission
import cz.alabanda.positionsharingapp.data.UserLocation
import cz.alabanda.positionsharingapp.repository.network.Result

/**
 * Define API for repository with data.
 */
interface Repository {

    /**
     * Get watched users.
     */
    suspend fun getReceivedPermissions(token: String): Result<List<Permission>?>

    /**
     * Login to an account.
     */
    suspend fun login(name: String, password: String): Result<String?>

    /**
     * Create new user.
     */
    suspend fun createUser(name: String, password: String): Result<Boolean>

    /**
     * Send location data
     */
    suspend fun sendLocation(token: String, time: String, latitude: String, longitude: String): Result<Boolean>

    /**
     * Request permission
     */
    suspend fun requestPermission(token: String, watchedUserName: String): Result<Boolean>

    /**
     * Cancel permission request
     */
    suspend fun cancelPermission(token: String, watchedUserName: String): Result<Boolean>

    suspend fun getUserPastLocations(token: String, userIds: List<Int>, maxResults: Int): Result<List<UserLocation>>

    suspend fun grantPermission(token: String, grantedUserName: String): Result<Boolean>

    suspend fun refusePermission(token: String, refusedUserName: String): Result<Boolean>

    suspend fun getGrantedPermissions(token: String): Result<List<Permission>?>
}