package cz.alabanda.positionsharingapp.fragments

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.observe
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import cz.alabanda.positionsharingapp.R
import cz.alabanda.positionsharingapp.repository.network.RestApiClient
import cz.alabanda.positionsharingapp.adapters.UsernameAndButtonAdapter
import cz.alabanda.positionsharingapp.data.Permission
import cz.alabanda.positionsharingapp.utils.ListComparer
import cz.alabanda.positionsharingapp.utils.ViewModelResponseState
import kotlinx.android.synthetic.main.fragment_watched_users.*
import java.util.*

class WantedUsersFragment : Fragment() {

    lateinit var receivedPermissionsAdapter: UsernameAndButtonAdapter

    /**
     * Inflate view hierarchy to this fragment.
     */
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_wanted_users, container, false)
    }

    override fun onStart() {
        super.onStart()
        receivedPermissionsAdapter = UsernameAndButtonAdapter(
            Collections.emptyList(),
            { cancelPermissionRequest(it) },
            getString(R.string.global_cancel)
        )

        wantedUsersView.adapter = receivedPermissionsAdapter
        wantedUsersView.layoutManager = LinearLayoutManager(requireContext())

        RestApiClient.prepareGetReceivedPermissions().observe(viewLifecycleOwner) {
            when (it) {
                is ViewModelResponseState.Error -> handleError(it.error)
                is ViewModelResponseState.Success -> handleWantedUsers(it.content)
            }
        }
        RestApiClient.getReceivedPermissions()
    }

    private fun handleWantedUsers(repositories: List<Permission>) {
        val oldData = receivedPermissionsAdapter.repositories
        val newData = repositories.filter { !it.approved }.map { user -> user.name }
        if (!ListComparer.isEqual(oldData, newData))
            receivedPermissionsAdapter.repositories = newData

    }

    private fun handleError(error: String) {
        val message = "Failed to download received permissions: $error"
        Log.i("TAG", message)
        val failure = Toast.makeText(
            context,
            message,
            Toast.LENGTH_SHORT
        )
        failure.show()
    }

    fun cancelPermissionRequest(username: String) {
        RestApiClient.prepareCancelPermission().observe(viewLifecycleOwner) {
            when (it) {
                is ViewModelResponseState.Error -> handlePermissionCancelError(it.error)
                is ViewModelResponseState.Success -> handlePermissionRequestCanceled()
            }
        }

        RestApiClient.cancelPermission(username)
    }

    private fun handlePermissionRequestCanceled() {
        // Show toast
        val success = Toast.makeText(context, "Permission request canceled", Toast.LENGTH_SHORT)
        success.show()

        // Refresh list
        RestApiClient.getReceivedPermissions()
    }

    private fun handlePermissionCancelError(error: String) {
        val message = "Permission request not canceled: $error"
        Log.i("TAG", message)
        // Show toast
        val failure = Toast.makeText(
            context,
            message,
            Toast.LENGTH_SHORT
        )
        failure.show()
    }

    fun requestPermission(username: String) {
        RestApiClient.prepareRequestPermission().observe(viewLifecycleOwner) {
            when (it) {
                is ViewModelResponseState.Error -> handlePermissionRequestError(it.error)
                is ViewModelResponseState.Success -> handlePermissionRequestSent()
            }
        }

        RestApiClient.requestPermission(username)
    }

    private fun handlePermissionRequestSent() {
        // Show toast
        val success = Toast.makeText(activity, "Permission request sent", Toast.LENGTH_SHORT)
        success.show()

        // Refresh list
        RestApiClient.getReceivedPermissions()
    }

    private fun handlePermissionRequestError(error: String) {
        val message = "Permission request not sent: $error"
        Log.i("TAG", message)
        // Show toast
        val failure = Toast.makeText(
            activity,
            message,
            Toast.LENGTH_SHORT
        )
        failure.show()
    }

    /**
     * Stop any [Glide] operations related to this fragment.
     */
    override fun onStop() {
        super.onStop()
        Glide.with(this).onStop()
    }

    companion object {

        /**
         * Factory method to create fragment instance. Framework requires empty default constructor.
         */
        @JvmStatic
        fun newInstance(): WantedUsersFragment {
            return WantedUsersFragment()
        }
    }
}