package cz.alabanda.positionsharingapp.data

import com.google.android.gms.maps.model.LatLng
import com.squareup.moshi.JsonClass
import java.time.LocalDateTime

@JsonClass(generateAdapter = true)
data class UserLocation(val userId: Int,
                        val time: String,
                        val latitude: Double,
                        val longitude: Double) {

    val localDateTime : LocalDateTime = LocalDateTime.parse(time)

    fun toLatLng() = LatLng(latitude, longitude)
}