package cz.alabanda.positionsharingapp.utils

import cz.alabanda.positionsharingapp.repository.network.RestApiService
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

object Provisions {

    val restApiRepository: RestApiService by lazy {
        val retrofit = Retrofit.Builder()
            .baseUrl("https://alabanda.cz:50443")
            .client(okHttpClient)
            .addConverterFactory(MoshiConverterFactory.create())
            .build()

        return@lazy retrofit.create(RestApiService::class.java)
    }

    private val okHttpClient: OkHttpClient by lazy {
        val builder = OkHttpClient.Builder()

        return@lazy builder.build()
    }
}