package cz.alabanda.positionsharingapp.fragments

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.lifecycle.observe
import com.bumptech.glide.Glide
import cz.alabanda.positionsharingapp.R
import cz.alabanda.positionsharingapp.repository.network.RestApiClient
import cz.alabanda.positionsharingapp.activities.WatchedUsersActivity
import cz.alabanda.positionsharingapp.utils.ViewModelResponseState
import kotlinx.android.synthetic.main.fragment_login.*
import java.math.BigInteger
import java.security.MessageDigest

/**
 * A simple [Fragment] subclass.
 * Use the [LoginFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class LoginFragment : Fragment() {

    private val SAVED_USERNAME_LABEL: String = "username"
    private val SAVED_PASSWORD_LABEL: String = "password"

    private var username: String? = ""
    private var password: String? = ""

    /**
     * Inflate view hierarchy to this fragment.
     */
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        username = savedInstanceState?.getString(SAVED_USERNAME_LABEL)
        password = savedInstanceState?.getString(SAVED_USERNAME_LABEL)
        return inflater.inflate(R.layout.fragment_login, container, false)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putString(SAVED_USERNAME_LABEL, (userNameEditText as EditText).text.toString())
        outState.putString(SAVED_PASSWORD_LABEL, (passwordText as EditText).text.toString())
    }

    override fun onStart() {
        super.onStart()

        registerButton.setOnClickListener { registerClicked() }
        loginButton.setOnClickListener { loginClicked() }

        userNameEditText.setText(username)
        passwordText.setText(password)

    }

    private fun registerClicked() {
        val username = (userNameEditText as EditText).text.toString()
        val password = (passwordText as EditText).text.toString()
        val hashedPassword = hash(password)

        RestApiClient.prepareUserCreate().observe(viewLifecycleOwner) {
            when (it) {
                ViewModelResponseState.Loading -> showCreatingUserProgress()
                is ViewModelResponseState.Error -> handleCreateUserError(it.error)
                is ViewModelResponseState.Success -> handleUserCreated()
            }
        }
        RestApiClient.createUser(username, hashedPassword)
    }

    private fun handleUserCreated() {
        // Show toast
        val success = Toast.makeText(activity, "Account created", Toast.LENGTH_SHORT)
        success.show()
        hideCreatingUserProgress()
    }

    private fun handleCreateUserError(error: String) {
        val message = "Account not created: $error"
        Log.i("TAG", message)
        // Show toast
        val failure = Toast.makeText(
            activity,
            message,
            Toast.LENGTH_SHORT
        )
        failure.show()
        hideCreatingUserProgress()
    }

    private fun showCreatingUserProgress() {
        (registerButton as Button).text = getString(R.string.global_registering)
    }

    private fun hideCreatingUserProgress() {
        (registerButton as Button).text = getString(R.string.global_register)
    }

    private fun loginClicked() {
        val username = (userNameEditText as EditText).text.toString()
        val password = (passwordText as EditText).text.toString()
        val hashedPassword = hash(password)

        RestApiClient.prepareLogin().observe(viewLifecycleOwner) {
            when (it) {
                ViewModelResponseState.Loading -> showLoggingProgress()
                is ViewModelResponseState.Error -> handleLoginError(it.error)
                is ViewModelResponseState.Success -> handleLogin()
            }
        }
        RestApiClient.login(username, hashedPassword)
    }

    private fun handleLogin() {
        // Show toast
        val success = Toast.makeText(activity, "Login successful", Toast.LENGTH_SHORT)
        success.show()

        // Create intent to open watched users activity
        val intent = Intent(activity, WatchedUsersActivity::class.java)
        startActivity(intent)
        hideLoggingProgress()
    }

    private fun handleLoginError(error: String) {
        val message = "Login failed: $error"
        Log.i("TAG", message)
        // Show toast
        val failure = Toast.makeText(
            activity,
            message,
            Toast.LENGTH_SHORT
        )
        failure.show()
        hideLoggingProgress()
    }

    private fun showLoggingProgress() {
        (loginButton as Button).text = getString(R.string.global_logging)
    }

    private fun hideLoggingProgress() {
        (loginButton as Button).text = getString(R.string.global_login)
    }

    private fun hash(pass: String): String {
        val bytes = pass.toByteArray()
        val md = MessageDigest.getInstance("MD5")
        return BigInteger(1, md.digest(bytes)).toString(16).padStart(32, '0')
    }

    /**
     * Stop any [Glide] operations related to this fragment.
     */
    override fun onStop() {
        super.onStop()
        Glide.with(this).onStop()
    }

    companion object {

        /**
         * Factory method to create fragment instance. Framework requires empty default constructor.
         */
        @JvmStatic
        fun newInstance(): LoginFragment {
            return LoginFragment()
        }
    }
}