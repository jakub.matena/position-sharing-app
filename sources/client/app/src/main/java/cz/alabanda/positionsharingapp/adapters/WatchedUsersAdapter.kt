package cz.alabanda.positionsharingapp.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.CheckBox
import android.widget.TextView
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import cz.alabanda.positionsharingapp.R
import cz.alabanda.positionsharingapp.data.Permission
import cz.alabanda.positionsharingapp.fragments.WatchedUsersFragment

class WatchedUsersAdapter(repositories: List<Permission>) :
    RecyclerView.Adapter<WatchedUsersAdapter.ViewHolder>() {

    var context: Context? = null
    // Set of all checked users in the list
    var checkedUsersIds = mutableSetOf<Int>()

    // All the users in the list
    var repositories = repositories
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    /**
     * Provide a reference to the type of views that you are using
     * (custom ViewHolder).
     */
    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val usernameTextView: TextView = view.findViewById(R.id.usernameTextView)
        val addToMapCheckBox: CheckBox = view.findViewById(R.id.addToMapCheckBox)
        val removeButton: Button = view.findViewById(R.id.removeButton)
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.watched_view_item, viewGroup, false)

        context = view.context
        return ViewHolder(view)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        val name = repositories[position].name
        viewHolder.usernameTextView.text = name
        viewHolder.addToMapCheckBox.setOnCheckedChangeListener{_, isChecked -> onCheckedChange(isChecked, repositories[position]) }

        val watchedUsersFragment = (context as FragmentActivity).supportFragmentManager.findFragmentById(
            R.id.watched_fragment_container
        ) as WatchedUsersFragment
        viewHolder.removeButton.setOnClickListener() {watchedUsersFragment.removeWatchedUser(name)}
    }

    private fun onCheckedChange(checked: Boolean, user: Permission) {
        if (checked) {
            checkedUsersIds.add(user.id)
        } else {
            checkedUsersIds.remove(user.id)
        }
    }

    override fun getItemCount() = repositories.size

    fun getWatchedUsersIds() : Set<Int> {
        return checkedUsersIds
    }
}
