package cz.alabanda.positionsharingapp.fragments

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.observe
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import cz.alabanda.positionsharingapp.R
import cz.alabanda.positionsharingapp.repository.network.RestApiClient
import cz.alabanda.positionsharingapp.adapters.WatchedUsersAdapter
import cz.alabanda.positionsharingapp.data.Permission
import cz.alabanda.positionsharingapp.utils.ListComparer
import cz.alabanda.positionsharingapp.utils.ViewModelResponseState
import kotlinx.android.synthetic.main.fragment_watched_users.*
import java.util.*

/**
 * A simple [Fragment] subclass.
 * Use the [WatchedUsersFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class WatchedUsersFragment : Fragment() {


    lateinit var watchedUsersAdapter: WatchedUsersAdapter

    /**
     * Inflate view hierarchy to this fragment.
     */
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_watched_users, container, false)
    }

    override fun onStart() {
        super.onStart()
        watchedUsersAdapter = WatchedUsersAdapter(Collections.emptyList())

        wantedUsersView.adapter = watchedUsersAdapter
        wantedUsersView.layoutManager = LinearLayoutManager(requireContext())

        RestApiClient.prepareGetReceivedPermissions().observe(viewLifecycleOwner) {
            when (it) {
                is ViewModelResponseState.Error -> handleError(it.error)
                is ViewModelResponseState.Success -> handleWatchedUsers(it.content)
            }
        }
        RestApiClient.getReceivedPermissions()
    }

    private fun handleWatchedUsers(repositories: List<Permission>) {
        val oldData = watchedUsersAdapter.repositories
        val newData = repositories.filter { it.approved }
        if (!ListComparer.isEqual(oldData, newData))
            watchedUsersAdapter.repositories = newData
    }

    private fun handleError(error: String) {
        val message = "Failed to download received permissions: $error"
        Log.i("TAG", message)
        val failure = Toast.makeText(
            context,
            message,
            Toast.LENGTH_SHORT
        )
        failure.show()
    }

    fun removeWatchedUser(username: String) {
        RestApiClient.prepareCancelPermission().observe(viewLifecycleOwner) {
            when (it) {
                is ViewModelResponseState.Error -> handleRemoveWatchedError(it.error)
                is ViewModelResponseState.Success -> handleWatchedRemoved()
            }
        }

        RestApiClient.cancelPermission(username)
    }

    private fun handleWatchedRemoved() {
        // Show toast
        val success = Toast.makeText(context, "Watched user removed", Toast.LENGTH_SHORT)
        success.show()

        // Refresh list
        RestApiClient.getReceivedPermissions()
    }

    private fun handleRemoveWatchedError(error: String) {
        val message = "Watched user not removed: $error"
        Log.i("TAG", message)
        // Show toast
        val failure = Toast.makeText(
            context,
            message,
            Toast.LENGTH_SHORT
        )
        failure.show()
    }

    /**
     * Stop any [Glide] operations related to this fragment.
     */
    override fun onStop() {
        super.onStop()
        Glide.with(this).onStop()
    }

    companion object {

        /**
         * Factory method to create fragment instance. Framework requires empty default constructor.
         */
        @JvmStatic
        fun newInstance(): WatchedUsersFragment {
            val fragment = WatchedUsersFragment()

            return fragment
        }
    }

    fun getWatchedUsersIds() : Set<Int> {
        return watchedUsersAdapter.getWatchedUsersIds()
    }
}

