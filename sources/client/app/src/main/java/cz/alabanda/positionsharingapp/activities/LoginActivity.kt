package cz.alabanda.positionsharingapp.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import cz.alabanda.positionsharingapp.R
import cz.alabanda.positionsharingapp.repository.network.RestApiClient
import cz.alabanda.positionsharingapp.fragments.LoginFragment

class LoginActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        supportFragmentManager.beginTransaction().add(
                R.id.login_fragment_container,
                LoginFragment.newInstance()
        ).commit()
    }
}