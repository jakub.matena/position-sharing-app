package cz.alabanda.positionsharingapp.fragments

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.observe
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import cz.alabanda.positionsharingapp.R
import cz.alabanda.positionsharingapp.repository.network.RestApiClient
import cz.alabanda.positionsharingapp.adapters.UsernameAndButtonAndButtonAdapter
import cz.alabanda.positionsharingapp.data.Permission
import cz.alabanda.positionsharingapp.utils.ListComparer
import cz.alabanda.positionsharingapp.utils.ViewModelResponseState
import kotlinx.android.synthetic.main.fragment_permission_requests.*
import kotlinx.android.synthetic.main.fragment_watched_users.*
import java.util.*

class PermissionRequestsFragment : Fragment() {

    lateinit var permissionRequestsAdapter: UsernameAndButtonAndButtonAdapter

    /**
     * Inflate view hierarchy to this fragment.
     */
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_permission_requests, container, false)
    }

    override fun onStart() {
        super.onStart()
        permissionRequestsAdapter = UsernameAndButtonAndButtonAdapter(
            Collections.emptyList(),
            { acceptPermissionRequest(it) },
            { refusePermissionRequest(it) },
            getString(R.string.global_accept),
            getString(R.string.global_refuse)
        )

        permissionRequestsView.adapter = permissionRequestsAdapter
        permissionRequestsView.layoutManager = LinearLayoutManager(requireContext())

        RestApiClient.prepareGetGrantedPermissions().observe(viewLifecycleOwner) {
            when (it) {
                is ViewModelResponseState.Error -> handleError(it.error)
                is ViewModelResponseState.Success -> handlePermissionsRequests(it.content)
            }
        }
        RestApiClient.getGrantedPermissions()
    }

    private fun handlePermissionsRequests(repositories: List<Permission>) {
        val oldData = permissionRequestsAdapter.repositories
        val newData = repositories.filter { !it.approved }.map { user -> user.name }
        if (!ListComparer.isEqual(oldData, newData))
            permissionRequestsAdapter.repositories = newData
    }

    private fun handleError(error: String) {
        val message = "Failed to download granted permissions: $error"
        Log.i("TAG", message)
        val failure = Toast.makeText(
            context,
            message,
            Toast.LENGTH_SHORT
        )
        failure.show()
    }

    fun acceptPermissionRequest(username: String) {
        RestApiClient.prepareGrantPermission().observe(viewLifecycleOwner) {
            when (it) {
                is ViewModelResponseState.Error -> handlePermissionRequestAcceptError(it.error)
                is ViewModelResponseState.Success -> handlePermissionRequestAccepted()
            }
        }

        RestApiClient.grantPermission(username)
    }

    private fun handlePermissionRequestAccepted() {
        // Show toast
        val success = Toast.makeText(context, "Permission request accepted", Toast.LENGTH_SHORT)
        success.show()

        // Refresh list
        RestApiClient.getGrantedPermissions()
    }

    private fun handlePermissionRequestAcceptError(error: String) {
        val message = "Permission request acceptation failed: $error"
        Log.i("TAG", message)
        // Show toast
        val failure = Toast.makeText(
            context,
            message,
            Toast.LENGTH_SHORT
        )
        failure.show()
    }

    fun refusePermissionRequest(username: String) {
        RestApiClient.prepareRefusePermission().observe(viewLifecycleOwner) {
            when (it) {
                is ViewModelResponseState.Error -> handleRefusePermissionError(it.error)
                is ViewModelResponseState.Success -> handlePermissionRequestRefused()
            }
        }

        RestApiClient.refusePermission(username)
    }

    private fun handlePermissionRequestRefused() {
        // Show toast
        val success = Toast.makeText(context, "Permission request refused", Toast.LENGTH_SHORT)
        success.show()

        // Refresh list
        RestApiClient.getGrantedPermissions()
    }

    private fun handleRefusePermissionError(error: String) {
        val message = "Permission request refusal failed: $error"
        Log.i("TAG", message)
        // Show toast
        val failure = Toast.makeText(
            context,
            message,
            Toast.LENGTH_SHORT
        )
        failure.show()
    }

    fun grantPermission(username: String) {
        RestApiClient.prepareGrantPermission().observe(viewLifecycleOwner) {
            when (it) {
                is ViewModelResponseState.Error -> handleGrantPermissionError(it.error)
                is ViewModelResponseState.Success -> handlePermissionGranted()
            }
        }

        RestApiClient.grantPermission(username)
    }

    private fun handlePermissionGranted() {
        // Show toast
        val success = Toast.makeText(activity, "Permission granted", Toast.LENGTH_SHORT)
        success.show()

        // Refresh list
        RestApiClient.getGrantedPermissions()
    }

    private fun handleGrantPermissionError(error: String) {
        val message = "Permission not granted: $error"
        Log.i("TAG", message)
        // Show toast
        val failure = Toast.makeText(
            activity,
            message,
            Toast.LENGTH_SHORT
        )
        failure.show()
    }

    /**
     * Stop any [Glide] operations related to this fragment.
     */
    override fun onStop() {
        super.onStop()
        Glide.with(this).onStop()
    }

    companion object {

        /**
         * Factory method to create fragment instance. Framework requires empty default constructor.
         */
        @JvmStatic
        fun newInstance(): PermissionRequestsFragment {
            return PermissionRequestsFragment()
        }
    }
}