package cz.alabanda.positionsharingapp.repository.network

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import cz.alabanda.positionsharingapp.data.Permission
import cz.alabanda.positionsharingapp.data.UserLocation
import cz.alabanda.positionsharingapp.repository.Repository
import cz.alabanda.positionsharingapp.utils.ViewModelResponseState
import kotlinx.coroutines.launch
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter


object RestApiClient : ViewModel() {
    private var token = ""
    private var dateFormatter = DateTimeFormatter.ofPattern("MM/dd/yyyy HH:mm:ss")
    private val repository: Repository by lazy { NetRepository() }

    private val grantedPermissions: MutableLiveData<ViewModelResponseState<List<Permission>, String>> by lazy {
        MutableLiveData<ViewModelResponseState<List<Permission>, String>>().apply {
            value = ViewModelResponseState.Idle
        }
    }

    fun prepareGetGrantedPermissions(): LiveData<ViewModelResponseState<List<Permission>, String>> {
        return grantedPermissions
    }

    // Pull all users that I am able to watch
    fun getGrantedPermissions() {
        viewModelScope.launch {
            grantedPermissions.value = ViewModelResponseState.Loading

            when(val grantedPermissions = repository.getGrantedPermissions(token)) {
                is Result.Success -> if (grantedPermissions.value != null) this@RestApiClient.grantedPermissions.value = ViewModelResponseState.Success(grantedPermissions.value)
                else -> this@RestApiClient.grantedPermissions.value = ViewModelResponseState.Error("Cannot pull granted permissions")
            }
        }
    }

    private val permissionGranted: MutableLiveData<ViewModelResponseState<Boolean, String>> by lazy {
        MutableLiveData<ViewModelResponseState<Boolean, String>>().apply {
            value = ViewModelResponseState.Idle
        }
    }

    fun prepareGrantPermission(): LiveData<ViewModelResponseState<Boolean, String>> {
        return permissionGranted
    }

    fun grantPermission(grantedUserName: String) {
        viewModelScope.launch {
            permissionGranted.value = ViewModelResponseState.Loading

            when(val isGrantedSent = repository.grantPermission(token, grantedUserName)) {
                is Result.Success -> permissionGranted.value = ViewModelResponseState.Success(isGrantedSent.value)
                else -> permissionGranted.value = ViewModelResponseState.Error("Failed to grant permission")
            }
        }
    }

    private val permissionRefused: MutableLiveData<ViewModelResponseState<Boolean, String>> by lazy {
        MutableLiveData<ViewModelResponseState<Boolean, String>>().apply {
            value = ViewModelResponseState.Idle
        }
    }

    fun prepareRefusePermission(): LiveData<ViewModelResponseState<Boolean, String>> {
        return permissionRefused
    }


    fun refusePermission(refusedUserName: String) {
        viewModelScope.launch {
            permissionRefused.value = ViewModelResponseState.Loading

            when(val isRefusalSent = repository.refusePermission(token, refusedUserName)) {
                is Result.Success -> permissionRefused.value = ViewModelResponseState.Success(isRefusalSent.value)
                else -> permissionRefused.value = ViewModelResponseState.Error("Failed to refuse permission")
            }
        }
    }

    private val userPastLocations: MutableLiveData<ViewModelResponseState<List<UserLocation>, String>> by lazy {
        MutableLiveData<ViewModelResponseState<List<UserLocation>, String>>().apply {
            value = ViewModelResponseState.Idle
        }
    }

    fun getUsersPastLocations(): LiveData<ViewModelResponseState<List<UserLocation>, String>> {
        return userPastLocations
    }

    fun pullUsersPastLocations(userIds: List<Int>) {
        val maxResults = 10 //TODO: future plan move to button/configuration?

        viewModelScope.launch {
            userPastLocations.value = ViewModelResponseState.Loading

            when(val result = repository.getUserPastLocations(token, userIds, maxResults)) {
                is Result.Success -> userPastLocations.value = ViewModelResponseState.Success(result.value)
                else -> userPastLocations.value = ViewModelResponseState.Error("Cannot pull user(s) locations.")
            }
        }
    }

    private val permissionCanceled: MutableLiveData<ViewModelResponseState<Boolean, String>> by lazy {
        MutableLiveData<ViewModelResponseState<Boolean, String>>().apply {
            value = ViewModelResponseState.Idle
        }
    }

    fun prepareCancelPermission(): LiveData<ViewModelResponseState<Boolean, String>> {
        return permissionCanceled
    }

    fun cancelPermission(watchedUserName: String) {
        viewModelScope.launch {
            permissionCanceled.value = ViewModelResponseState.Loading

            when(val isCancelSent = repository.cancelPermission(token, watchedUserName)) {
                is Result.Success -> permissionCanceled.value = ViewModelResponseState.Success(isCancelSent.value)
                else -> permissionCanceled.value = ViewModelResponseState.Error("Cannot remove permission")
            }
        }
    }

    private val permissionRequested: MutableLiveData<ViewModelResponseState<Boolean, String>> by lazy {
        MutableLiveData<ViewModelResponseState<Boolean, String>>().apply {
            value = ViewModelResponseState.Idle
        }
    }

    fun prepareRequestPermission(): LiveData<ViewModelResponseState<Boolean, String>> {
        return permissionRequested
    }

    fun requestPermission(watchedUserName: String) {
        viewModelScope.launch {
            permissionRequested.value = ViewModelResponseState.Loading

            when(val isRequestSent = repository.requestPermission(token, watchedUserName)) {
                is Result.Success -> permissionRequested.value = ViewModelResponseState.Success(isRequestSent.value)
                else -> permissionRequested.value = ViewModelResponseState.Error("Cannot send permission request")
            }
        }
    }

    private val locationSent: MutableLiveData<ViewModelResponseState<Boolean, String>> by lazy {
        MutableLiveData<ViewModelResponseState<Boolean, String>>().apply {
            value = ViewModelResponseState.Idle
        }
    }

    fun prepareLocationSend(): LiveData<ViewModelResponseState<Boolean, String>> {
        return locationSent
    }

    fun sendLocation(latitude: Double, longitude: Double) {
        viewModelScope.launch {
            locationSent.value = ViewModelResponseState.Loading
            val time = LocalDateTime.now().format(dateFormatter).toString()

            when(val isLocationSent = repository.sendLocation(token, time, latitude.toString(), longitude.toString())) {
                is Result.Success -> locationSent.value = ViewModelResponseState.Success(isLocationSent.value)
                else -> locationSent.value = ViewModelResponseState.Error("Cannot send location data")
            }
        }
    }

    private val userCreated: MutableLiveData<ViewModelResponseState<Boolean, String>> by lazy {
        MutableLiveData<ViewModelResponseState<Boolean, String>>().apply {
            value = ViewModelResponseState.Idle
        }
    }

    fun prepareUserCreate(): LiveData<ViewModelResponseState<Boolean, String>> {
        return userCreated
    }

    fun createUser(name: String, password: String) {
        viewModelScope.launch {
            userCreated.value = ViewModelResponseState.Loading

            when(val isUserCreated = repository.createUser(name, password)) {
                is Result.Success -> userCreated.value = ViewModelResponseState.Success(isUserCreated.value)
                else -> userCreated.value = ViewModelResponseState.Error("Cannot create account")
            }
        }
    }

    private val _token: MutableLiveData<ViewModelResponseState<String, String>> by lazy {
        MutableLiveData<ViewModelResponseState<String, String>>().apply {
            value = ViewModelResponseState.Idle
        }
    }

    fun prepareLogin(): LiveData<ViewModelResponseState<String, String>> {
        return _token
    }

    fun login(name: String, password: String) {
        viewModelScope.launch {
            _token.value = ViewModelResponseState.Loading

            when(val loginToken = repository.login(name, password)) {
                is Result.Success -> {
                    if (loginToken.value != null) {
                        _token.value = ViewModelResponseState.Success(loginToken.value)
                        token = loginToken.value
                    }
                }
                else -> _token.value = ViewModelResponseState.Error("Cannot login")
            }
        }
    }

    private val receivedPermissions: MutableLiveData<ViewModelResponseState<List<Permission>, String>> by lazy {
        MutableLiveData<ViewModelResponseState<List<Permission>, String>>().apply {
            value = ViewModelResponseState.Idle
        }
    }

    fun prepareGetReceivedPermissions(): LiveData<ViewModelResponseState<List<Permission>, String>> {
        return receivedPermissions
    }

    // Pull all users that I am able to watch
    fun getReceivedPermissions() {
        viewModelScope.launch {
            receivedPermissions.value = ViewModelResponseState.Loading

            when(val receivedPermissions = repository.getReceivedPermissions(token)) {
                is Result.Success -> if (receivedPermissions.value != null) this@RestApiClient.receivedPermissions.value = ViewModelResponseState.Success(receivedPermissions.value)
                else -> this@RestApiClient.receivedPermissions.value = ViewModelResponseState.Error("Cannot pull received permissions")
            }
        }
    }

    fun logout() {
        token = ""
    }
}