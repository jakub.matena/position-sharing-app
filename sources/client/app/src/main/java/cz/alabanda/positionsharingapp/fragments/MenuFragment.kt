package cz.alabanda.positionsharingapp.fragments

import android.app.Activity
import android.content.Intent
import android.content.Intent.FLAG_ACTIVITY_CLEAR_TASK
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.IntentCompat
import cz.alabanda.positionsharingapp.R
import cz.alabanda.positionsharingapp.activities.LoginActivity
import cz.alabanda.positionsharingapp.activities.MapActivity
import cz.alabanda.positionsharingapp.activities.PermissionsActivity
import cz.alabanda.positionsharingapp.activities.WatchedUsersActivity
import cz.alabanda.positionsharingapp.repository.network.RestApiClient
import cz.alabanda.positionsharingapp.services.LiveDataService
import cz.alabanda.positionsharingapp.services.LocationService
import kotlinx.android.synthetic.main.fragment_login.*
import kotlinx.android.synthetic.main.fragment_menu.*

/**
 * A simple [Fragment] subclass.
 * Use the [MenuFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class MenuFragment : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onStart() {
        super.onStart()

        watchedScreenButton.setOnClickListener { watchedClicked() }
        permissionsScreenButton.setOnClickListener { permissionsClicked() }
        logoutButton.setOnClickListener { logoutClicked() }
    }

    private fun watchedClicked() {
        val intent = Intent(activity, WatchedUsersActivity::class.java)
        startActivity(intent)
    }

    private fun permissionsClicked() {
        val intent = Intent(activity, PermissionsActivity::class.java)
        startActivity(intent)
    }

    private fun logoutClicked() {
        RestApiClient.logout()
        val ls = Intent(activity, LocationService::class.java)
        activity?.stopService(ls)
        val ds = Intent(activity, LiveDataService::class.java)
        activity?.stopService(ds)

        val i = Intent(activity, LoginActivity::class.java)        // Specify any activity here e.g. home or splash or login etc
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        i.putExtra("EXIT", true)
        startActivity(i)
        (activity as Activity).finish()
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_menu, container, false)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @return A new instance of fragment MenuFragment.
         */
        @JvmStatic
        fun newInstance() =
            MenuFragment().apply {
                arguments = Bundle().apply {
                }
            }
    }
}