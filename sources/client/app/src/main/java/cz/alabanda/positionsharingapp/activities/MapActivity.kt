package cz.alabanda.positionsharingapp.activities

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.observe
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.maps.model.PolylineOptions
import cz.alabanda.positionsharingapp.R
import cz.alabanda.positionsharingapp.data.UserLocation
import cz.alabanda.positionsharingapp.repository.network.RestApiClient
import cz.alabanda.positionsharingapp.services.LiveMapDataService
import cz.alabanda.positionsharingapp.utils.ColorPicker
import cz.alabanda.positionsharingapp.utils.ViewModelResponseState
import kotlinx.android.synthetic.main.activity_maps.*
import java.time.format.DateTimeFormatter

class MapActivity : AppCompatActivity(), OnMapReadyCallback {
    companion object {
        const val watchedUsersIdsExtraName = "watchedUsersIdsExtraName"
    }

    private lateinit var ids : ArrayList<Int>
    private var dateFormatter = DateTimeFormatter.ofPattern("HH:mm:ss (dd.MM.)")
    private val colorPicker = ColorPicker()
    private var initialZoomPerformed = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)

        processIntent()
        startLiveDataService()

        map.onCreate(savedInstanceState)
        map.onResume()
        map.getMapAsync(this)
    }

    private fun processIntent() {
        ids = intent.getIntegerArrayListExtra(watchedUsersIdsExtraName) ?: ArrayList()
    }

    override fun onDestroy() {
        super.onDestroy()
        stopLiveDataService()
    }

    override fun onMapReady(googleMap: GoogleMap?) {
        googleMap?.apply {
            RestApiClient.getUsersPastLocations().observe(this@MapActivity) {
                if (it is ViewModelResponseState.Success)
                    onWatchedUserLocationChange(it.content)
                if (it is ViewModelResponseState.Error)
                    throw Error("Error while loading user data.")
            }

            RestApiClient.pullUsersPastLocations(ids)
        }
    }

    private fun startLiveDataService() {
        val intent = Intent(this, LiveMapDataService::class.java)
        intent.putIntegerArrayListExtra(watchedUsersIdsExtraName, ArrayList(ids))
        startService(intent)
    }

    private fun stopLiveDataService() {
        val liveMapDataService = Intent(this, LiveMapDataService::class.java)
        stopService(liveMapDataService)
    }

    private fun GoogleMap.onWatchedUserLocationChange(locations: List<UserLocation>) {
        removeOldMarkers()
        addNewMarkers(locations)
        addNewPolylines(locations)
        zoomInIfFirstLocationChange(locations)
    }

    private fun GoogleMap.zoomInIfFirstLocationChange(locations: List<UserLocation>) {
        if (locations.isNotEmpty() && !initialZoomPerformed) {
            initialZoomPerformed = true
            zoomIn(locations)
        }
    }

    private fun GoogleMap.removeOldMarkers() { clear() }

    private fun GoogleMap.addNewMarkers(locations: List<UserLocation>) {
        for (location in locations)
            addMarkerToMap(location)
    }

    private fun GoogleMap.addNewPolylines(locations: List<UserLocation>) {
        for (id in ids)
            addPolylineToMap(locations, id)
    }

    private fun GoogleMap.zoomIn(locations: List<UserLocation>) {
        val zoom = 15.0f
        val zoomAnimationLength = 200

        animateCamera(
            CameraUpdateFactory.newLatLngZoom(locations.first().toLatLng(), zoom),
            zoomAnimationLength,
            null
        )
    }

    private fun GoogleMap.addMarkerToMap(location: UserLocation) {
        val position = getPosition(location.userId)

        addMarker(
            MarkerOptions()
                .position(location.toLatLng())
                .icon(BitmapDescriptorFactory.defaultMarker(colorPicker.getHue(position)))
                .title("User ${location.userId} at ${dateFormatter.format(location.localDateTime) }.")
        )
    }

    private fun GoogleMap.addPolylineToMap(locations: List<UserLocation>, id: Int) {
        val options = PolylineOptions()
        options.color(colorPicker.getColor(getPosition(id)))

        for (location in locations)
            if (location.userId == id)
                options.add(location.toLatLng())

        addPolyline(options)
    }

    private fun getPosition(id: Int): Int {
        return ids.indexOf(id)
    }
}