package cz.alabanda.positionsharingapp.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import cz.alabanda.positionsharingapp.R
import cz.alabanda.positionsharingapp.dialogs.UsernameDialogFragment
import cz.alabanda.positionsharingapp.fragments.GrantedPermissionsFragment
import cz.alabanda.positionsharingapp.fragments.MenuFragment
import cz.alabanda.positionsharingapp.fragments.PermissionRequestsFragment
import cz.alabanda.positionsharingapp.fragments.WantedUsersFragment
import kotlinx.android.synthetic.main.activity_permissions.*

class PermissionsActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_permissions)


        supportFragmentManager.beginTransaction().add(
            R.id.menu_fragment_container,
            MenuFragment.newInstance()
        ).commit()
        supportFragmentManager.beginTransaction().add(
            R.id.granted_permissions_fragment_container,
            GrantedPermissionsFragment.newInstance()
        ).commit()
        supportFragmentManager.beginTransaction().add(
            R.id.permissions_requests_fragment_container,
            PermissionRequestsFragment.newInstance()
        ).commit()

        grantPermissionFAB.setOnClickListener { showDialog() }
    }

    private fun showDialog() {
        val permissionsRequestsFragment = supportFragmentManager.findFragmentById(R.id.permissions_requests_fragment_container) as PermissionRequestsFragment
        val newFragment = UsernameDialogFragment (
            { username: String -> permissionsRequestsFragment.grantPermission(username) },
            getString(R.string.grant_permission_label),
            getString(R.string.grant_permission_accept_button_text),
            getString(R.string.grant_permission_cancel_button_text)
        )
        newFragment.show(supportFragmentManager, "request")
    }
}