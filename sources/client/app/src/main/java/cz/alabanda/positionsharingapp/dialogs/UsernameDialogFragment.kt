package cz.alabanda.positionsharingapp.dialogs

import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.widget.EditText
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentActivity
import cz.alabanda.positionsharingapp.R


class UsernameDialogFragment(private val action: (m: String) -> Unit, private val dialogLabel: String, private val acceptText: String, private val cancelText: String) : DialogFragment() {

    private var fragmentActivity: FragmentActivity? = null
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return activity?.let {
            // Use the Builder class for convenient dialog construction
            val builder = AlertDialog.Builder(it)

            fragmentActivity = it
            val factory = LayoutInflater.from(it)
            val view = factory.inflate(R.layout.dialog_username, null);

            builder.setView(view)
            builder.setMessage(dialogLabel)
                .setPositiveButton(
                    acceptText
                ) { _, _ ->
                    val username =
                        (view.findViewById(R.id.username) as EditText).text.toString()
                    action(username)
                }
                .setNegativeButton(
                    cancelText
                ) { _, _ ->
                    // Just do nothing
                }
            // Create the AlertDialog object and return it
            builder.create()
        } ?: throw IllegalStateException("Activity cannot be null")
    }
}
