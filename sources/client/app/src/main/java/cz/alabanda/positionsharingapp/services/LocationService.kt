package cz.alabanda.positionsharingapp.services

import android.Manifest
import android.app.*
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.os.IBinder
import android.os.Looper
import android.util.Log
import androidx.core.app.ActivityCompat
import com.google.android.gms.location.*
import cz.alabanda.positionsharingapp.R
import cz.alabanda.positionsharingapp.repository.network.RestApiClient

class LocationService : Service() {

    companion object {
        private const val intervalBetweenLocationUpdatesInMs : Long = 5000

        fun locationRequest(): LocationRequest {
            return LocationRequest.create().apply {
                interval = intervalBetweenLocationUpdatesInMs
                fastestInterval = intervalBetweenLocationUpdatesInMs / 2
                priority = LocationRequest.PRIORITY_HIGH_ACCURACY
            }
        }
    }

    val mLocationCallback =
        object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult?) {
                locationResult ?: return
                for (location in locationResult.locations){
                    Log.i("TAG", "New location recorded: ${location.latitude}, ${location.longitude}")
                    RestApiClient.sendLocation(location.latitude, location.longitude)
                }
            }
        }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        startOnForeground()
        startLocationUpdates()

        return START_NOT_STICKY;
    }

    override fun onDestroy() {
        super.onDestroy()
        stopLocationUpdates()
    }

    private fun startOnForeground() {
        val notification = createForegroundNotification()
        startForeground(1, notification)
    }

    private fun createForegroundNotification(): Notification {
        val notificationChannel = createNotificationChannel()
        val pendingIntent: PendingIntent =
            Intent(this, LocationService::class.java).let { notificationIntent ->
                PendingIntent.getActivity(this, 0, notificationIntent, 0)
            }

        return Notification.Builder(this, notificationChannel)
            .setContentTitle("Position sharing is active.")
            //.setContentText("Position sharing is active.")
            .setSmallIcon(R.drawable.gps_icon)
            .setContentIntent(pendingIntent)
            .build()
    }

    private fun stopLocationUpdates() {
        val locationClient = LocationServices.getFusedLocationProviderClient(this);
        locationClient.removeLocationUpdates(mLocationCallback)
    }

    private fun startLocationUpdates() {
        val locationClient = LocationServices.getFusedLocationProviderClient(this);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            locationClient.requestLocationUpdates(
                locationRequest(),
                mLocationCallback,
                Looper.getMainLooper()
            )
        }
    }

    private fun createNotificationChannel(): String{
        val channelId = "Position sharing active"
        val chan = NotificationChannel(channelId, "Position sharing active", NotificationManager.IMPORTANCE_LOW)
        chan.lightColor = Color.BLUE
        chan.lockscreenVisibility = Notification.VISIBILITY_PRIVATE

        val service = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        service.createNotificationChannel(chan)

        return channelId
    }

    override fun onBind(p0: Intent?): IBinder? {
        return null
    }
}