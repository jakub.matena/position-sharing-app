﻿using Microsoft.Extensions.Configuration;
using System;

namespace db
{
    class Program
    {
        static void Main(string[] args)
        {
            var configuration = new ConfigurationBuilder()
            .SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
            .AddJsonFile("secrets.json")
            .AddUserSecrets<Program>()
            .Build();

            var password = configuration["DatabasePassword"];

            new ScriptRunner().ExecuteScripts(password);
        }
    }
}
