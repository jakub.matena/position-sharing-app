USE SharedPositions;

DROP TABLE IF EXISTS Users
CREATE TABLE Users (
	id INT IDENTITY(1,1) PRIMARY KEY,
	name VARCHAR(50) UNIQUE,
	password VARCHAR(50),
	token CHAR(20) UNIQUE
)

DROP TABLE IF EXISTS Locations
CREATE TABLE Locations (
	id BIGINT IDENTITY(1,1) PRIMARY KEY,
	time DATETIME DEFAULT GETDATE(),
	userId INT,
	latitude FLOAT,
	longitude FLOAT
)

DROP TABLE IF EXISTS Permissions
CREATE TABLE Permissions (
	id INT IDENTITY(1,1) PRIMARY KEY,
	watchedId INT,
	watcherId INT,
	approved BIT
)
