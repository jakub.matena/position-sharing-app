﻿using System;
using System.Data.SqlClient;
using System.IO;

namespace db
{
    class ScriptRunner
    {
        public void ExecuteScripts(string password)
        {
            foreach (var file in Directory.GetFiles("scripts"))
            {
                var reader = new StreamReader(file);
                ExecuteSql(reader.ReadToEnd(), password);
            }

            Console.WriteLine("Scripts finished.");
        }

        private void ExecuteSql(string sql, string password)
        {
            var connetionString = @"Data Source=alabanda.cz, 1433; User ID=SA;Password=" + password;
            var connection = new SqlConnection(connetionString);

            connection.Open();
            new SqlCommand(sql, connection).ExecuteNonQuery();
            connection.Close();
        }
    }
}
