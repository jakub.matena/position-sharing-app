﻿using Dapper;
using server.Locations.InputObjects;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace server.Locations
{
    public class LocationRepository
    {
        private readonly IDbConnection connection;

        public LocationRepository(IDbConnection connection)
        {
            this.connection = connection;
        }

        public async Task<int> InsertNewLocation(NewLocationInput input, int userId)
        {
            return await connection.ExecuteAsync("INSERT INTO Locations VALUES (@time, @userId, @latitude, @longitude)",
                new { input.Time, userId, input.Latitude, input.Longitude });
        }

        public async Task<List<Location>> GetLocations(GetLocationInput input)
        {
            var locations = await connection.QueryAsync<Location>(
                "SELECT * FROM ( " +
                "SELECT *, row_number() over (partition by userId order by time desc) as rank " +
                "FROM Locations) tbl " +
                "WHERE tbl.rank <= @MaxResults AND userId IN @watchedUserIds",
                new { input.WatchedUserIds, input.MaxResults });

            return locations.AsList();
        }
    }
}
