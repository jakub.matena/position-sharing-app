﻿using System;
using System.Collections.Generic;

namespace server.Locations.InputObjects
{
    public class GetLocationInput
    {
        public string Token { get; set; }

        public List<int> WatchedUserIds { get; set; } = new List<int>();

        public int MaxResults { get; set; }

        public override string ToString() => $"Input requesting max {MaxResults} last locations of user(s) {string.Join(", ", WatchedUserIds)} for user with token {Token}.";
    }
}
