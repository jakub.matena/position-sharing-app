﻿using System;

namespace server.Locations.InputObjects
{
    public class NewLocationInput
    {
        public string Token { get; set; }

        public DateTime Time { get; set; }

        public float Latitude { get; set; }

        public float Longitude { get; set; }
    }
}
