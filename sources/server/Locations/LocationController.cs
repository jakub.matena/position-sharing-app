using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;
using System.Collections.Generic;
using server.Users;
using server.Locations.InputObjects;
using server.Permissions;
using Microsoft.AspNetCore.Http;

namespace server.Locations
{
    [ApiController]
    [Route("[controller]")]
    public class LocationController : ControllerBase
    {
        private readonly ILogger<LocationController> logger;
        private readonly LocationRepository locationRepository;
        private readonly UserRepository userRepository;
        private readonly PermissionRepository permissionRepository;

        public LocationController(ILogger<LocationController> logger, LocationRepository locationRepository, UserRepository userRepository, PermissionRepository permissionRepository)
        {
            this.logger = logger;
            this.locationRepository = locationRepository;
            this.userRepository = userRepository;
            this.permissionRepository = permissionRepository;
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        public async Task<ActionResult<List<Location>>> Get([FromQuery] GetLocationInput input)
        {
            logger.LogInformation($"Getting locations, input: {input}");

            if (input.MaxResults <= 0)
                return BadRequest();

            var watcher = await userRepository.Get(input.Token);

            if (watcher == null)
                return Unauthorized();

            if (!await permissionRepository.HasPermissions(watcher.Id, input.WatchedUserIds))
                return Forbid();

            return await locationRepository.GetLocations(input);
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<ActionResult> Create([FromQuery] NewLocationInput input)
        {
            logger.LogInformation($"Posting new user location.");

            var user = await userRepository.Get(input.Token);

            if (user == null)
                return Unauthorized();

            await locationRepository.InsertNewLocation(input, user.Id);
            return Ok();
        }
    }
}
