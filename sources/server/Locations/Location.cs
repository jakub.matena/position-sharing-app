﻿using System;

namespace server.Locations
{
    public class Location
    {
        public int UserId { get; set; }

        public DateTime Time { get; set; }

        public float Latitude { get; set; }

        public float Longitude { get; set; }
    }
}
