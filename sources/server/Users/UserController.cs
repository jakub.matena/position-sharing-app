using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Data;
using System.Threading.Tasks;
using System.Linq;
using System;
using Microsoft.AspNetCore.Http;

namespace server.Users
{
    [ApiController]
    [Route("[controller]")]
    public class UserController : ControllerBase
    {
        private readonly ILogger<UserController> logger;
        private readonly UserRepository userRepository;
        private readonly Random random = new Random();

        public UserController(ILogger<UserController> logger, UserRepository userRepository)
        {
            this.logger = logger;
            this.userRepository = userRepository;
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<User>> Get(string token)
        {
            logger.LogInformation($"Getting user with token {token}.");

            var user = await userRepository.Get(token);

            if (user == null)
                return NotFound();

            return user;
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status422UnprocessableEntity)]
        public async Task<ActionResult> Create(string name, string password)
        {
            logger.LogInformation($"Creating user with name {name} and password {password}.");

            if (await userRepository.Exists(name))
                return UnprocessableEntity();

            var token = await GenerateUniqueToken();
            await userRepository.Create(name, password, token);

            return Ok();
        }

        [Route("token")]
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [Produces("application/json")]
        public async Task<ActionResult<string>> Login(string name, string password)
        {
            logger.LogInformation($"Getting token for user with name {name} and password {password}.");

            var token = await GenerateUniqueToken();
            var rowsUpdated = await userRepository.UpdateToken(name, password, token);

            if (rowsUpdated == 0)
                return NotFound();

            return token;
        }

        private async Task<string> GenerateUniqueToken()
        {
            var token = GenerateToken();

            while (await userRepository.IsTokenInUse(token))
                token = GenerateToken();

            return token;
        }

        private string GenerateToken()
        {
            const string pool = "abcdefghijklmnopqrstuvwxyz0123456789";
            var chars = Enumerable.Range(0, 20).Select(x => pool[random.Next(0, pool.Length)]);

            return new string(chars.ToArray());
        }
    }
}
