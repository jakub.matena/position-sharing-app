﻿using Dapper;
using System.Data;
using System.Threading.Tasks;

namespace server.Users
{
    public class UserRepository
    {
        private readonly IDbConnection connection;

        public UserRepository(IDbConnection connection)
        {
            this.connection = connection;
        }

        public async Task<User> Get(string token)
        {
            return await connection.QuerySingleOrDefaultAsync<User>("SELECT * FROM Users WHERE token = @token",
                new { token });
        }

        public async Task<User> GetById(int id)
        {
            return await connection.QuerySingleOrDefaultAsync<User>("SELECT * FROM Users WHERE id = @id",
                new { id });
        }

        public async Task<User> GetByName(string name)
        {
            return await connection.QuerySingleOrDefaultAsync<User>("SELECT * FROM Users WHERE name = @name",
                new { name });
        }

        public async Task<bool> Exists(string name)
        {
            var result = await connection.QueryFirstOrDefaultAsync<string>("SELECT name FROM Users WHERE name = @name",
                new { name });

            return result != null;
        }

        public async Task<bool> Exists(int userId)
        {
            var result = await connection.QueryFirstOrDefaultAsync<string>("SELECT name FROM Users WHERE Id = @userId",
                new { userId });

            return result != null;
        }

        public async Task<int> Create(string name, string password, string token)
        {
            return await connection.ExecuteAsync("INSERT INTO Users VALUES(@name, @password, @token)",
                new { name, password, token });
        }

        public async Task<bool> IsTokenInUse(string token)
        {
            var result = await connection.QueryFirstOrDefaultAsync<string>("SELECT token FROM Users WHERE token = @token",
                new { token });

            return result != null;
        }

        public async Task<int> UpdateToken(string name, string password, string token)
        {
            return await connection.ExecuteAsync("UPDATE Users SET Token = @token WHERE name = @name AND password = @password",
                new { name, password, token });
        }
    }
}
