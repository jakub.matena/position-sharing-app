using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;
using server.Permissions;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using System;

namespace server.Users
{
    [ApiController]
    [Route("[controller]")]
    public class PermissionController : ControllerBase
    {
        private readonly ILogger<UserController> logger;
        private readonly PermissionRepository permissionRepository;
        private readonly UserRepository userRepository;

        public PermissionController(ILogger<UserController> logger, PermissionRepository permissionRepository, UserRepository userRepository)
        {
            this.logger = logger;
            this.permissionRepository = permissionRepository;
            this.userRepository = userRepository;
        }

        [Route("granted")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [HttpGet]
        public async Task<ActionResult<List<PermissionUser>>> GetGivenPermissions(string token)
        {
            logger.LogInformation($"Getting all granted permissions by user with token {token}.");

            var user = await userRepository.Get(token);

            if (user == null)
                return Unauthorized();

            return await permissionRepository.GetGrantedPermissions(user.Id);
        }

        [Route("granted/grant")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpPost]
        public async Task<ActionResult> GrantPermission(string token, string grantedUserName)
        {
            logger.LogInformation($"User with token {token} is granting permission to user with name {grantedUserName}.");

            var watchedUser = await userRepository.Get(token);

            if (watchedUser == null)
                return Unauthorized();

            var grantedUser = await userRepository.GetByName(grantedUserName);

            if (grantedUser == null)
                return NotFound();

            var rowsUpdated = await permissionRepository.GrantPermission(watchedUser, grantedUser);

            if (rowsUpdated == 0)
                return NotFound();

            return Ok();
        }

        [Route("granted/refuse")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpPost]
        public async Task<ActionResult> RefusePermission(string token, string refusedUserName)
        {
            logger.LogInformation($"User with token {token} is refusing permission to user with name {refusedUserName}.");

            var watchedUser = await userRepository.Get(token);

            if (watchedUser == null)
                return Unauthorized();

            var refusedUser = await userRepository.GetByName(refusedUserName);

            if (refusedUser == null)
                return Unauthorized();

            var rowsUpdated = await permissionRepository.RefusePermission(watchedUser, refusedUser);

            if (rowsUpdated == 0)
                return NotFound();

            return Ok();
        }

        [Route("received")]
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<ActionResult<List<PermissionUser>>> GetRecievedPermissions(string token)
        {
            logger.LogInformation($"Getting all received or requests permissins for user with token {token}.");

            var user = await userRepository.Get(token);

            if (user == null)
                return Unauthorized();

            return await permissionRepository.GetReceivedPermissions(user.Id);
        }

        [Route("received")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpPost]
        public async Task<ActionResult> RequestPermission(string token, string watchedUserName)
        {
            logger.LogInformation($"User with token {token} is requesting permission from user with username {watchedUserName}.");

            var watcher = await userRepository.Get(token);

            if (watcher == null)
                return Unauthorized();

            if (!await userRepository.Exists(watchedUserName))
                return NotFound();

            int watchedId = (await userRepository.GetByName(watchedUserName)).Id;

            var rowsUpdated = await permissionRepository.RequestPermission(watcher.Id, watchedId);

            if (rowsUpdated != 1)
                logger.LogInformation($"User with token {token} has already requested permission from user with username {watchedUserName}.");

            return Ok();
        }

        [Route("received/cancel")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpPost]
        /// Deletes permission given by watchedUserName to owner of the token no matter if the permission is still in the state of request.
        public async Task<ActionResult> CancelPermissionRequest(string token, string watchedUserName)
        {
            logger.LogInformation($"User with token {token} is canceling permission request for user with username {watchedUserName}.");

            var watcher = await userRepository.Get(token);

            if (watcher == null)
                return Unauthorized();

            if (!await userRepository.Exists(watchedUserName))
                return NotFound();

            var watched = await userRepository.GetByName(watchedUserName);

            var rowsUpdated = await permissionRepository.RefusePermission(watched, watcher);

            if (rowsUpdated != 1)
                throw new Exception("Shouldn't happen.. (Hello there!)");

            return Ok();
        }
    }
}
