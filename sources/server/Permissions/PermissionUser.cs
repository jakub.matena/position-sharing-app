namespace server.Permissions
{
    public class PermissionUser
    {
        public int id { get; set; }

        public string name { get; set; }

        public bool approved {get;set;}
    }
}
