﻿using Dapper;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using server.Users;

namespace server.Permissions
{
    public class PermissionRepository
    {
        private readonly IDbConnection connection;
        private readonly UserRepository userRepository;

        public PermissionRepository(IDbConnection connection, UserRepository userRepository)
        {
            this.connection = connection;
            this.userRepository = userRepository;
        }

        private async Task<bool> PermissionExists(int watchedId, int watcherId)
        {
            var count = await connection.QuerySingleAsync<int>("SELECT COUNT(*) FROM Permissions WHERE watcherId = @watcherId AND watchedId = @watchedId",
                new { watchedId, watcherId });

            return count != 0;
        }

        public async Task<int> GrantPermission(User watchedUser, User grantedUser)
        {
            var watchedId = watchedUser.Id;
            var grantedId = grantedUser.Id;

            // Deletes old records
            await RefusePermission(watchedUser, grantedUser);

            // And creates new one
            return await connection.ExecuteAsync(
                    "INSERT INTO Permissions VALUES (@watchedId, @grantedId, 1)",
                    new { watchedId, grantedId });
        }

        public async Task<int> RefusePermission(User watchedUser, User refusedUser)
        {
            var watchedId = watchedUser.Id;
            var refusedId = refusedUser.Id;

            return await connection.ExecuteAsync(
                "DELETE FROM Permissions WHERE watchedId = @watchedId AND watcherId = @refusedId",
                new { watchedId, refusedId });
        }

        public async Task<int> RequestPermission(int watcherId, int watchedId)
        {
            if(!await PermissionExists(watchedId, watcherId))
            {
                return await connection.ExecuteAsync("INSERT INTO Permissions VALUES (@watchedId, @watcherId, 0)",
                new { watchedId, watcherId });
            }
            return 0;      
        }

        public async Task<bool> HasPermissions(int watcherId, List<int> watchedIds)
        {
            var approvedCount = await connection.QuerySingleAsync<int>("SELECT COUNT(*) FROM Permissions WHERE watcherId = @watcherId AND watchedId IN @watchedIds AND approved = 1",
                new { watchedIds, watcherId });

            return approvedCount == watchedIds.Count;
        }

        public async Task<List<PermissionUser>> GetReceivedPermissions(int userId) {

            var permissions = await connection.QueryAsync<Permission>("SELECT * FROM Permissions WHERE watcherId = @userId",
                new { userId });

            var permissionUsers = new List<PermissionUser>();
            foreach (var permission in permissions)
            {
                var permissionUserId = permission.WatchedId;
                var permissionUserApproved = permission.Approved;
                var permissionUser = await userRepository.GetById(permissionUserId);
                permissionUsers.Add(
                    new PermissionUser() {id = permissionUserId, name = permissionUser.Name, approved = permissionUserApproved
                    });
            }

            return permissionUsers;
        }

        public async Task<List<PermissionUser>> GetGrantedPermissions(int userId) {

            var permissions = await connection.QueryAsync<Permission>("SELECT * FROM Permissions WHERE watchedId = @userId",
                new { userId });

            var permissionUsers = new List<PermissionUser>();
            foreach (var permission in permissions)
            {
                var permissionUserId = permission.WatcherId;
                var permissionUserApproved = permission.Approved;
                var permissionUser = await userRepository.GetById(permissionUserId);
                permissionUsers.Add(
                    new PermissionUser() {id = permissionUserId, name = permissionUser.Name, approved = permissionUserApproved
                    });
            }

            return permissionUsers;
        }
    }
}
