﻿namespace server.Permissions
{
    public class Permission
    {
        public int WatchedId { get; set; }

        public int WatcherId { get; set; }

        public bool Approved { get; set; }
    }
}
