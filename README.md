# Position Sharing App


## Client

## Server  
  
### ASP.NET Core Application  
Created from Dockerfile located in sources/server/Dockerfile  
cd sources/server  
docker build -t alabanda.cz/position-sharing-app/server .  

docker volume create position-sharing-app-https
docker volume inspect position-sharing-app-https # Copy certificate to volume

docker run --name position-sharing-app-server -e Secrets_DatabasePassword=databasePassword -e ASPNETCORE_Kestrel__Certificates__Default__Path=/root/.dotnet/https/ssl.pfx -e ASPNETCORE_Kestrel__Certificates__Default__Password=certificatePassword -e ASPNETCORE_URLS=https://alabanda.cz:443/ -v position-sharing-app-https:/root/.dotnet/https -p 5080:80 -p 50443:443 --restart unless-stopped alabanda.cz/position-sharing-app/server # Port forward to not colide with other apps running on the server
  
### Microsoft SQL Server Database

Guidelines to create docker and hello world can be found here: https://docs.microsoft.com/en-us/sql/linux/quickstart-install-connect-docker?view=sql-server-ver15&pivots=cs1-bash  
  


sudo docker run -e "ACCEPT_EULA=Y" -e "SA_PASSWORD=password" -p 1433:1433 --restart unless-stopped -v sqldata1:/var/opt/mssql --name sql1 -h sql1 -d mcr.microsoft.com/mssql/server:2019-latest  

docker exec -it sql1 "bash"  
/opt/mssql-tools/bin/sqlcmd -S localhost -U SA -P "password"

### Database password
secrets.json file should be located in folder like C:\Users\<User>\AppData\Roaming\Microsoft\UserSecrets\<UserSecretsId>  
UserSecretsId can be found in server.csproj under PropertyGroup > UserSecretsId

## Script runner

### Database password
secrets.json file should be located in the root project directory and should be automatically copied when application is built  
The file should look like this:  
{  
  "DatabasePassword": "password"  
}